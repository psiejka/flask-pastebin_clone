from flask import Blueprint, render_template, request
from models import Code, db

admin = Blueprint('admin', __name__, template_folder='templates')


@admin.route('/', methods=['GET'])
def index():
    codes = Code.query.all()
    return render_template("admin_panel.html", codes=codes)
