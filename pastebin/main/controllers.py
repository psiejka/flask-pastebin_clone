from flask import Blueprint, render_template, request, redirect, url_for
from models import Code, db

main = Blueprint('main', __name__, template_folder='templates')


@main.route('/', methods=['GET', 'POST'])
def index():
    errors = []
    results = {}
    if request.method == "POST":
        try:
            title = request.form['title']
            code = request.form['code']
            language = request.form['language']
        except:
            errors.append("Something went terribly wrong. Please try again.")
        if code:
            if not title:
                title = "Untitled"
            registry = Code(code, language, title)
            db.session.add(registry)
            db.session.commit()

            code = Code.query.filter(Code.id == registry.id).first()
            return redirect(url_for('.view', code_id=code.id))
    return render_template("index.html", errors=errors, results=results)


@main.route('/<int:code_id>', methods=['GET'])
def view(code_id):
    errors = []
    code = Code.query.filter(Code.id == code_id).first()
    if not code:
        errors.append("There is no such code with this ID.")
        return render_template('index.html', errors=errors, code=code)
    return render_template('viewer.html', errors=errors, code=code)
