from flask import Flask
from pastebin.main.controllers import main
from pastebin.admin.controllers import admin
from models import db

app = Flask(__name__,
            template_folder='templates')
app.config.from_object('config.DevelopmentConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)
from models import Code

app.jinja_env.add_extension('jinja2.ext.loopcontrols')

app.register_blueprint(main, url_prefix='/')
app.register_blueprint(admin, url_prefix='/admin')

if __name__ == '__main__':
    app.run()
