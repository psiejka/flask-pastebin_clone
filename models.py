from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Code(db.Model):
    __tablename__ = 'code'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), default="Untitled")
    code = db.Column(db.String(5000))
    language = db.Column(db.String(60))

    def __init__(self, code, language, title="Untitled"):
        self.title = title
        self.code = code
        self.language = language

    def __repr__(self):
        return f'{self.title}'
