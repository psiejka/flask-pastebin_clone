import os
basedir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    DB_PASS = os.environ["DB_PASS"]
    SQLALCHEMY_DATABASE_URI = f'postgresql://postgres:{DB_PASS}@localhost/pastebin_clone__dev'


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True


class TestingConfig(BaseConfig):
    TESTING = True
