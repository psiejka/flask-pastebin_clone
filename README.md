# Flask based clone of Pastebin

This project was my first try with Flask framework. The goal was simple: build small clone of popular website where we can store text/code snippets.

* Database used: PostreSQL.
* Code editor with syntax highlighting: [Ace (Ajax.org Cloud9 Editor)](https://github.com/ajaxorg/ace).

If you want to run this project you will have to download Ace code editor and paste its source code to *static/js/* directory.

# References
* [Flask framework documentation](https://flask.palletsprojects.com/en/1.1.x/)
* [Flask Series by Damyan Bogoev](https://damyanon.net/tags/flask-series/)
* [Flask by Example Series by Real Python](https://realpython.com/flask-by-example-part-1-project-setup/)

# Images

![index]( images/index_min.png "Index")
![code_snippet](images/code_snippet_min.png "Code Snippet")
